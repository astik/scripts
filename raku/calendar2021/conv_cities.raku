#!/usr/bin/raku

use JSON::Tiny;

my Str $cities = './json/cities.json'.IO.slurp;
my @data = from-json $cities;
my @row;
my @out;
my %h;
my Int $i;

my @keys = ['name', 'slug', 'timezone', 'ru', 'geoname_id'];

{
    %h = @data[0][$i++];
    @row = [];

    for @keys -> $k {
        @row.push(%h{$k});
    }
    
    @out.push(@row.join(';'));
} for ^@data[0].elems;
@out .= sort;
@out.unshift: @keys.join(';');

'cities.csv'.IO.spurt: @out.join("\n");
'done.'.say;
