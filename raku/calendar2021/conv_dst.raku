#!/usr/bin/raku

use JSON::Tiny;

my $dst_file = './json/dst.json';
my $content = $dst_file.IO.slurp;
my Str $line;
my %hash;
my @dst = from-json $content;
my @keys = ['slug', 'year', 'starts', 'ends', 'starts2', 'ends2'];
my @vals = [];
my @output;
my $output_file = './csv/dst.csv';

loop (my $i = 0; $i < @dst[0].elems; $i++){
    %hash = @dst[0][$i];
    @vals = [];
    @vals.push(%hash{$_} ?? %hash{$_} !! '') for @keys;
    say @vals;
    @output.push(@vals.join(';'));
}

if !($output_file.IO ~~ :e) {
    $output_file.IO.open(:create, :rw).close;
}

$output_file.IO.spurt: @output.join("\n");
'done.'.say;




