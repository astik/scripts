#!/usr/bin/raku

my $files = qqx/find -name '*.mp4'/;
my @lines = $files.lines.sort;
my Str $idx;
my Str $new;

@lines.map({$_ .= subst('./')});

for @lines -> $line {
    $idx = $line.substr(0..$line.index('-')).chop;
    $new = 'new/' ~ $idx ~ '.mp4';

    qqx/ffmpeg -i $line -i $pic -map 0 -map 1 -c copy -c:v:1 jpg -disposition:v:1 attached_pic $new/;

}
