#!/usr/bin/raku

my Str $new_name;
for dir() -> $file {
    next if $file.Str ~~ /list|ext|work|index\.html$/;
    $new_name = $file.Str;
    $new_name ~~ s:g/index\.html_//;
    $new_name ~~ s:g/\=//;
    rename $file.IO, $new_name;
}
put 'done.';
