#!/usr/bin/raku

my @lines = './json/masas.csv'.IO.lines;
my @headers = @lines.shift.split(';');
my @masas;
my @parts;
my %h;

for @lines -> $line {
    @parts = $line.split(';');
    %h := {};
    for ^@headers.elems {
        %h.push(@headers[$_] => @parts[$_]);
    }
    @masas.push(%h);
}

@masas.grep(*<slug> eq 'govinda')[0]<ru>.say;
