#!/usr/bin/raku

my $out = qqx<find "/media/astik/data/Авадхут Махарадж" -type f -name '*.mp4'>;
my $command;
my $outname;
for $out.lines -> $file {
    $outname = $file.IO.basename.substr(0, *-4);
    $command = qq<ffmpeg -i "$file" -vn -ab 192K "mp3/{$outname}.mp3">;
    shell $command;
    #qqx<ffmpeg "$file" -vn -acodec copy mp3/"{$file.IO.basename ~ '.mp3'}">
}
