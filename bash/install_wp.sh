#!/bin/bash

# Default options
LOCALE="ru_RU"
DB_HOST="localhost"

printf "Name of the project? cf My Project: "
read PROJECT_NAME

printf "Slug of the project? cf myproject: "
read PROJECT_SLUG

printf "Shortname of the project? cf mp: "
read PROJECT_PREFIX

printf "Admin email:"
read ADMIN_EMAIL

printf "Admin name:"
read ADMIN_NAME

printf "WordPress admin user password: "
read ADMIN_PASSWORD

printf "Site url:"
read SITE_URL

printf "Install path:"
read INSTALL_PATH

if [ -f $PROJECT_SLUG.sql ]; then
  rm -f $PROJECT_SLUG.sql
fi

touch $PROJECT_SLUG.sql
echo "drop database if exists $PROJECT_SLUG;" >> $PROJECT_SLUG.sql
echo "drop user if exists $PROJECT_SLUG@localhost;" >> $PROJECT_SLUG.sql
echo "create database $PROJECT_SLUG;" >> $PROJECT_SLUG.sql
echo "create user $PROJECT_SLUG@localhost identified by '$PROJECT_SLUG';" >> $PROJECT_SLUG.sql
echo "grant all privileges on $PROJECT_SLUG.* to $PROJECT_SLUG@localhost;" >> $PROJECT_SLUG.sql
mysql < $PROJECT_SLUG.sql

# Install WordPress and create the wp-config.php file...
wp core download --path=$INSTALL_PATH --locale=$LOCALE
wp core config --path=$INSTALL_PATH --dbname=$PROJECT_SLUG --dbuser=$PROJECT_SLUG --dbpass=$PROJECT_SLUG --dbhost=$DB_HOST --dbprefix=$PROJECT_PREFIX"_"
wp core install --path=$INSTALL_PATH --title="$PROJECT_NAME" --url=$SITE_URL --admin_user="$ADMIN_NAME" --admin_email="$ADMIN_EMAIL" --admin_password="$ADMIN_PASSWORD"

# Go in the install directory
if [ ! -d "$INSTALL_PATH" ]; then
  mkdir -p "$INSTALL_PATH"
fi
cd $INSTALL_PATH

# Update WordPress options
wp option update permalink_structure '/%category%/%postname%/'
wp option update default_ping_status 'closed'
wp option update default_pingback_flag '0'
wp post delete 1
wp post delete 2
wp comment delete 1


# Install and activate defaults plugins
wp plugin install cyr2lat stops-core-theme-and-plugin-updates updraftplus wordpress-seo regenerate-thumbnails wp-super-cache --activate

# Update plugins
wp plugin update --all
