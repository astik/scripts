#!/usr/bin/raku

my $files = qqx/find . -name '*.mp4'/;
my @lines = $files.lines;
@lines.map( {$_ .= subst('.mp4'); $_ .= subst('./')});

for @lines -> $line {
    my $input = "{$line}.mp4";
    my $audio = "{$line}.wav";
    my $outname = "out/{$line}.mp4";
    qqx/ffmpeg -y -i $input -i $audio -c:v copy -map 0:v:0 -map 1:a:0 $outname/;
}

