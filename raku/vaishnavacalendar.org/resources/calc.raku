#!/usr/bin/raku

my %months := {
    <Mar> => <03>,
    <Apr> => <04>,
    <May> => <05>,
    <Jun> => <06>,
    <Jul> => <07>,
    <Aug> => <08>,
    <Sep> => <09>,
    <Oct> => <10>,
    <Nov> => <11>,
    <Dec> => <12>,
    <Jan> => <01>,
    <Feb> => <02>
};


my Int constant $india_tz = (5.5 * 3600).Int;

my @nakshatras = ['Śrāvaṇ','Rohini','Punarvasu', 'Pūṣya'];

sub make_date(Str $date) {
    my @parts = $date.split('-');
    return Date.new(
        <day> => @parts[2],
        <month> => @parts[1],
        <year> => @parts[0]
    );
}

sub datetime_to_str(DateTime $dt) {
    return $dt.yyyy-mm-dd ~ ' ' ~ $dt.hh-mm-ss.chop(3);
}

sub next_date_from_str(Str $date){
    return Date.new($date).later(:1day).Str;
}

sub get_arunoday(Str $datetime, Int $tz = $india_tz){
    return '' unless $datetime;

    my Str ($arunoday_date, $arunoday_time);
    my DateTime $arunoday_dt;
    my Str ($y, $m, $d, $hh, $mm);

    ($arunoday_date, $arunoday_time) = $datetime.words.List;
    ($y, $m, $d) = $arunoday_date.split('-').List;
    ($hh, $mm) = $arunoday_time.split(':').List;
    $arunoday_dt = make_datetime($y, $m, $d, $hh, $mm, $tz);
    $arunoday_dt .= earlier(:96minutes);
    return $arunoday_dt.yyyy-mm-dd ~ ' ' ~ $arunoday_dt.hh-mm-ss.chop(3);
}

sub get_srss_line(Str $city, Str $date) {
    my @result;
    my @source;
    my Str $year = $date.substr(0..3);

    @source = "./srss/{$city}_{$year}.csv".IO.lines;
    @result = @source.grep(*.contains($date));
    return @result[0];
}

sub get_sunrise(Str $city, Str $date) {
    my $srss_line = get_srss_line($city, $date);
    return '' unless $srss_line;
    return $srss_line.split(';')[1];
}

sub get_sunset(Str $city, Str $date) {
    my $srss_line = get_srss_line($city, $date);
    return '' unless $srss_line;
    return $srss_line.split(';')[2];
}

sub get_navadvip_sunrise($date, $year) {
    my @result;
    my @parts;
    my @tithis = "src/navadvip_{$year}.csv".IO.lines;
    my ($hh, $ss);
    my %special_months = %months.invert;
    my $special_date = $date.substr(8..9).Int ~ ';'
        ~ %special_months{$date.substr(5..6)} ~ ';'
        ~ $date.substr(0..3);
    @result = @tithis.grep(*.contains($special_date));
    @parts = @result[0].split(';');
    ($hh, $ss) = @parts[10].split(':');
    return $date ~ ' ' ~ sprintf('%.2d', $hh.Int) ~ ':' ~ sprintf('%.2d', $ss.Int);
}

sub make_datetime($y, $m, $d, $hh, $mm, $tz) {
    return DateTime.new(
        year => $y,
        month => $m,
        day => $d,
        hour => $hh,
        minute => $mm,
        timezone => $tz
    );
}

sub get_dst_data(Str $city, Int $year) {
    my $dst_times = "csv/dst.csv";
    my $line = $dst_times.IO.lines.grep(*.contains("$city;$year"))[0];
    my %data;
    my @parts = $line.split: ';';
    %data<dst1_start> = @parts[2];
    %data<dst1_end> = @parts[3];
    %data<dst2_start> = @parts[4];
    %data<dst2_end> = @parts[5];
    return %data;
}

sub get_city_tz(Str $city) {
    my Str $line = "csv/cities.csv".IO.lines.grep(*.contains($city))[0];
    return $line.split(';')[5] * 3600;
}

sub MAIN(Str $city, Int $year) {

my Str constant $kshaya = 'kshaya';
my Str constant $sampurna = 'sampurna';
my Str constant $shuddha = 'shuddha';
my Str constant $viddha = 'viddha';
my Str constant $dvadashi = 'Dvādaśī';
my Str constant $ekadashi = 'Ekādaśī';
my Str constant $navadvip = 'navadvip';
my Str $tithis = "src/navadvip_{$year}.csv";

my Str $exceptions = 'csv/exceptions.csv';
my Str $dst_times = 'csv/dst.csv';
my Str @parts;
my Str @lines;
my Str @output;
my @row;
my Str (@next_parts, @next_hhmm);
my @hhmm;
my DateTime ($end_dt, $nakshatra_dt);
my Str ($line, $next_line, $prev_line);
my Int ($d, $m, $y, $hh, $mm);
my Bool ($dst, $dst_offset);
my Str ($start_tithi, $end_tithi, $date, $type);
my Str ($start_sunrise, $next_day_sunrise, $sunset);
my Str $arunoday;
my Str ($sunrise_date, $next_sunrise_date, $sunrise_for_arunoday);
my Str ($start_tithi_date, $next_day_date, $end_tithi_date);
my Str ($nakshatra_name, $nakshatra_start, $nakshatra_end);
my Bool $is_exception;
my %dst;

my $city_tz = get_city_tz($city);
%dst = get_dst_data($city, $year);
@lines = $tithis.IO.lines;

loop ( my $i = 0; $i < @lines.elems; $i++ ) { #main cycles

    $line = @lines[$i]; #current row in tithi table

    @parts = $line.split(';');
    @hhmm = @parts[4].split(':');
    $end_dt = make_datetime(@parts[7], %months{@parts[6]}, @parts[5], @hhmm[0], @hhmm[1], $india_tz);
    $end_dt .= in-timezone($city_tz) unless $city_tz == $india_tz;

#dst

    if (%dst{'dst1_start'}) {
        $dst_offset = %dst{'dst1_start'} le $end_dt.yyyy-mm-dd le %dst{'dst1_end'} ||
            %dst{'dst2_start'} le $end_dt.yyyy-mm-dd le %dst{'dst2_end'};
        $end_dt .= later: :1hour if $dst_offset;
    }

#calculating start and end time for tithi

    $start_tithi = '';

    if $i {
        $prev_line = @output[$i - 1];
        $start_tithi = $prev_line.split(';')[4];
    }

    $end_tithi = datetime_to_str($end_dt);

#calculating two sunrises times - it is quite enough

    $start_sunrise = '';
    $next_day_sunrise = '';

    $sunrise_date = $i ?? $start_tithi.words[0] !! $end_tithi.words[0];
    #for first tithi in list we get time of end fo sunrise

    $next_sunrise_date = next_date_from_str($sunrise_date);

    $start_sunrise = $city eq $navadvip ?? get_navadvip_sunrise($sunrise_date, $year)
        !! get_sunrise($city, $sunrise_date);

    if (0 < $i < @lines.elems - 1) {
        $next_day_sunrise = $city eq $navadvip ?? get_navadvip_sunrise($next_sunrise_date, $year)
            !! get_sunrise($city, $next_sunrise_date);
    }

# calculating sunsets

   $sunset = '';
   if $city eq $navadvip {
        if @parts[11] {
            $sunset = $sunrise_date ~ ' ' ~ @parts[11];
        }
   }
   else {
        $sunset = get_sunset($city, $sunrise_date);
   }

# nakshatras

    $nakshatra_name = '';
    $nakshatra_start = '';
    $nakshatra_end = '';
    if @parts[17] {
        $nakshatra_name = @parts[17].Str;
        $d = @parts[12].Int;
        $m = %months{@parts[13]}.Int;
        $y = @parts[14].Int;
        @hhmm = @parts[19].split(':');
        $hh = @hhmm[0].Int;
        $mm = @hhmm[1].Int;
        $nakshatra_dt = make_datetime($y, $m, $d, $hh, $mm, $india_tz);
        $nakshatra_dt .= in-timezone($city_tz) unless $city_tz == $india_tz;
        $nakshatra_start = datetime_to_str($nakshatra_dt);
        $next_line = @lines[$i+1];
        @next_parts = $next_line.split(';');
        @next_hhmm = @next_parts[19].split(':');
        $y = @next_parts[14].Int;
        $m = %months{@next_parts[13]}.Int;
        $d = @next_parts[12].Int;
        $nakshatra_dt = make_datetime($y, $m, $d, @next_hhmm[0].Int, @next_hhmm[1].Int, $india_tz);
        $nakshatra_dt .= in-timezone($city_tz) unless $city_tz == $india_tz;
        $nakshatra_end = datetime_to_str($nakshatra_dt);
    }

# calculating date of tithi in Gregorian

    $start_tithi_date = $start_tithi ?? $start_tithi.words[0] !! '';
    $next_day_date = $next_sunrise_date;
    $end_tithi_date = $end_tithi.words[0];

    $date = $start_tithi_date ?? $start_tithi_date !! $end_tithi_date;

    $type = $shuddha;

    if ($start_tithi gt $start_sunrise && $end_tithi ge $next_day_sunrise) {
        $date = $next_day_date;
    }

    if ($start_tithi && ($start_tithi le $start_sunrise)) {
        $date = $start_tithi_date;
    }

    if ($start_tithi && ($start_tithi gt $start_sunrise &&
        $end_tithi lt $next_day_sunrise)) {
        $type = $kshaya;
        $is_exception = False;
        for $exceptions.IO.lines {
           $is_exception = $line.contains($_);
           last if $is_exception;
        }
        $date = $is_exception ?? $start_tithi_date !! $next_day_date;
    }

    if ($next_day_sunrise && ($end_tithi ge $next_day_sunrise &&
        $start_tithi le $start_sunrise)) {
        $type = $sampurna;
        $date = $next_day_date if @parts[2].contains($ekadashi);
    }

#calculating arunoday

    $arunoday = '';
    if $sunrise_date {
        $sunrise_for_arunoday = $start_sunrise.contains($date) ??
        $start_sunrise !! $next_day_sunrise;
        $arunoday = get_arunoday($sunrise_for_arunoday);
    }
    $type = $viddha if $arunoday && ($arunoday lt $start_tithi) &&
        $type eq $shuddha;

#putting to file

    @row[$_] = @parts[$_] for 0..2;
    @row[3] = $start_tithi;
    @row[4] = $end_tithi;
    @row[5] = $start_sunrise;
    @row[6] = $next_day_sunrise;
    @row[7] = $sunset;
    @row[8] = $date;
    @row[9] = $arunoday;
    @row[10] = $type;
    @row[11] = $nakshatra_name;
    @row[12] = $nakshatra_start;
    @row[13] = $nakshatra_end;

    @output.push: @row.join(';');
}

    @output.unshift: "masa;paksha;tithi;start;end;start_sunrise;next_day_sunrise;sunset;date;arunoday;type;nakshatra_name;nakshatra_start;nakshatra_end";
    "tithis/{$city}_{$year}_tt.csv".IO.spurt: @output.join("\n");
    'done.'.say;

}
