#!/usr/bin/raku

my $png_files = qqx/find -name '*.png'/;
my $mp4_files = qqx/find -name '*.mp4' -maxdepth 1/;
my @png_lines = $png_files.lines;
my @mp4_lines = $mp4_files.lines;
my $clip_dir_name = 'clips';
my $fade_dir_name = 'fades';
my $out_dir_name = 'out';
my $amp4_dir_name = 'amp4';
my Str $idx;
my Str $thumb_name;
my Str $out_name;
my Str $mp4_name;
my Str $fade_name;
my Str $audiomp4_name;
my Str $basename;

@png_lines.map({$_ .= subst('./')});
@mp4_lines.map({$_ .= subst('./')});


for ($clip_dir_name, $fade_dir_name, $out_dir_name, $amp4_dir_name) -> $path {
    unless $path.IO.d {$path.IO.mkdir;}
}

for @png_lines -> $line {
    $basename = $line.subst('.png');
    $mp4_name = $clip_dir_name ~ '/' ~ $basename ~ '.mp4';
    $fade_name = $fade_dir_name ~ '/' ~ $basename ~ '.mp4';
    $audiomp4_name = $amp4_dir_name ~ '/' ~ $basename ~ '.mp4';

    qqx/ffmpeg -y -loop 1 -i $line -c:v libx264 -t 6 -pix_fmt yuv420p $mp4_name/;
    qqx/ffmpeg -y -i $mp4_name -vf "fade=type=out:duration=1:start_time=5" -c:a copy $fade_name/;
    qqx/ffmpeg -y -f lavfi -i aevalsrc=0 -i $fade_name -shortest -c:v copy -c:a mp3 -strict experimental $audiomp4_name/;
}

for @mp4_lines -> $line {
    $out_name = $out_dir_name ~ '/' ~ $line;
    $idx = $line.substr(0..$line.index('-')).chop;
    $thumb_name = $amp4_dir_name ~ '/' ~ $idx ~ '.mp4';
    qqx/ffmpeg -y -i $thumb_name -i $line -filter_complex "[0:v:0] [0:a:0] [1:v:0] [1:a:0] concat=n=2:v=1:a=1 [outv] [outa]" -map "[outv]" -map "[outa]" $out_name/;
}
