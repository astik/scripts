puts "Hello, I guess a number between 1 and 100. Can you guess it?"
target = rand(100) + 1
num_guesses = 0
puts "You have #{10 - num_guesses} guesses left."
guess_it = false
until guess_it || num_guesses == 10
  
  print "Enter the number: "
  num = gets.to_i

  if num < target
    puts "Oops, your number is too LOW"
    num_guesses += 1
    puts "You have #{10 - num_guesses} guesses left."
  elsif num > target
    puts "Oops, your number is too HIGH"
    num_guesses += 1
    puts "You have #{10 - num_guesses} guesses left."
  else
    puts "Congratulate! You win!"
    guess_it = true
  end
end

unless guess_it 
  puts "You loose! The number was #{target}" 
end

