#!/usr/bin/raku

my $list = './list.lst';
my Str @parts;
my Int $i = 0;
my Str ($from, $to, $out);

for $list.IO.lines -> $line {
    @parts = $line.split(' по ');
    @parts.map({$_ .= subst(/\#/, :g)});
    ($from, $to) = @parts;
    $out = "piece" ~ ++$i ~ ".mp4";
    qq:x/ffmpeg -y -i video.mp4 -ss $from -to $to -c:v copy -c:a copy $out/
}
