#!/usr/bin/raku

use JSON::Tiny;

my $json_file = './json/masas.json';
my @data = from-json $json_file.IO.slurp;
my @lines;
my @vals;
my %h;
my @keys = ['id','slug','indian_name','diacritic','en','ru'];
my $i;

{
    @vals = [];
    %h = @data[0][$i++];
    @vals.push: %h{$_} for @keys;
    @lines.push(@vals.join(';'));
} for ^@data[0].elems;
'./json/masas.csv'.IO.spurt: @lines.join("\n");
'done.'.say;
