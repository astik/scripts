#!/usr/bin/raku

my @list = dir().words;
my ( Str $content, @trs, @rows, @new_rows, @tds );

for @list -> $file {
    $content = $file.IO.slurp;
    @trs = $content ~~ m:g/\<tr\>.*?\<\/tr\>/;
    @rows.push: |@trs;
}

for [0 .. @rows.end] -> $idx {
    my $row = @rows[$idx];
    $row ~~ s:g/\<th\>(.*?)\<\/th\>//;
    $row ~~ s:g/\<tr\>\s+\<\/tr\>//;
    $row ~~ s:g/\n//;
    @tds = $row ~~ m:g/\<td\>.*?\<\/td\>/;
    for [0 .. @tds.end] -> $idx2 {
        my $td = @tds[$idx2];
        $td ~~ s:g/\<td\>(.*?)\<\/td\>/$0/;
        @tds[$idx2] = $td;
    }

    $row = @tds.join('|');
    @rows[$idx] = $row;
}

 my $fh = open 'baza.csv', :w;
 $fh.say($_) for @rows;
 $fh.close;
 say 'done.';

