#!/usr/bin/raku

my @a = [0..10] xx 5;
my @b;

@b[$_] = $_ + 1 for ^20;

say [*] 1..10;

.say for @a;
.say for @b;
