#!/usr/bin/raku

my $password = '';
$password ~= ( '0' .. 'z' ).pick(5).join('') for 1 .. 7;
say $password;
