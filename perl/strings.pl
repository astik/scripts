#!/usr/bin/perl
use strict;
use warnings;
use diagnostics;

my $x = "Hello";
my $y = "World";

#concatenation
my $z = $x . " " . $y; 
#or 
$z = "$x $y";

my $w = "Take " . ( 3 + 7);
print "$w\n";

# This is also possible for strings in perl
$z .= "! ";

#repetition
my $q = $z x 5;
print "$q\n";




