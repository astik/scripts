#!/usr/bin/raku

use Text::CSV;

sub MAIN ( Str $file )
{
    my $csv = Text::CSV.new;
    my $fh = open $file, :r, :!chomp;
    my Str $content;
    my Str $output;

    while (my $row = $csv.getline($fh)) {
        next if $row[0] ~~ /title/;
        $output ~= "<h1>{$row[0]}</h1>\n";
        $content = $row[1];
        $content ~~ s:g/\<.+?\>//;
        $content ~~ s:g/\&nbsp\;\—/\x20\—/;
        $content ~~ s:g/\&nbsp\;//;
        $content ~~ s:g/^\t//;
        for $content.lines -> $line {
            unless $line ~~ /s1|s3|FLV|flv|Код\x20для\x20вставки|Flash|SWFObject|скачать|player1|^$|\#.+\#/ {
                $output ~= "$line\n";
            }
        }
    }
    $output ~~ s:g/\n\n/\n/;
    $file.subst(/csv$/, "raku.txt").IO.spurt: $output;
    $fh.close;
}
