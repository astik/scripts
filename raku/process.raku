#!/usr/bin/raku

my Str $path = "/media/astik/home/astik/.local/share/work";
my Str $location = "/media/astik/home/astik/.local/share/scripts/raku";
my Str $dirname;

my @lines = qqx/find $path -name '*.raku' -type f/.lines;
my Str ($file_from, $file_to);

for @lines -> $file_from {
    $file_to = $location ~ "/" ~ $file_from.subst($path ~ "/");
    $dirname = $file_to.IO.dirname;
    unless $dirname.IO.d {mkdir $dirname.IO};
    "Copying from $file_from to $file_to".say;
    copy $file_from, $file_to;
}
